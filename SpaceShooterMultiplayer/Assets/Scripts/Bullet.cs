using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float lifetime = 2;
    public float moveForce = 4;


    private Rigidbody rigidbody;

    #region lifecycle
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, lifetime);
    }

    void Update()
    {
        rigidbody.velocity = transform.forward * moveForce;
    }
    #endregion
}
