using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [Header("UI")]
    public GameObject nameCanvas;
    public Text nameText;
    public Vector3 nameCanvasOffset;


    [Header("Balancing")]
    public float moveForce = 450;
    public float rotationSpeed = 450;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;

    private Camera camera;
    private Rigidbody rigidbody;


    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        
        RotateToMouse();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.AddForce(transform.forward * Time.deltaTime * moveForce);
        }
        if (Input.GetMouseButtonDown(0))
        {
            spawnBullet();
        }

        moveNameCanvas();
        moveCamera();
    }

    void RotateToMouse()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (true)
        {
            Vector3 hitPoint = ray.GetPoint(100);
            hitPoint.y = 1;
            Vector3 direction = hitPoint - transform.position;
            direction.y = 0;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        }
    }

    void moveCamera()
    {
        camera.gameObject.transform.position = transform.position + Vector3.up;
    }

    void moveNameCanvas()
    {
        nameCanvas.transform.position = transform.position + Vector3.forward;
    }
        

    public void spawnBullet()
    {
        GameObject bullet = Instantiate<GameObject>(bulletPrefab, bulletSpawnPoint.position, transform.rotation);
    }
    
}
